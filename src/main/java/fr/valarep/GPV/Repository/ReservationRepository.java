package fr.valarep.GPV.Repository;

import fr.valarep.GPV.Model.Reservation;
import fr.valarep.GPV.Model.User;
import fr.valarep.GPV.Model.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {


   public Collection<Reservation> getAllByUser_Id(Long id);
   public Collection<Reservation> getAllByDateReservationAfter(LocalDateTime dateReservation);
}
