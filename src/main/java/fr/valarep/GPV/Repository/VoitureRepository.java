package fr.valarep.GPV.Repository;

import fr.valarep.GPV.Model.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoitureRepository extends JpaRepository<Voiture, Long> {
    Voiture findPersonById(Long id);
}
