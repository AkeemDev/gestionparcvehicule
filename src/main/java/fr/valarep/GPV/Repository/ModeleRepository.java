package fr.valarep.GPV.Repository;

import fr.valarep.GPV.Model.Modele;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModeleRepository extends JpaRepository<Modele ,Long> {


}
