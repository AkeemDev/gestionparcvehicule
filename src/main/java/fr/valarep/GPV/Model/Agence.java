package fr.valarep.GPV.Model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Agence {

    @Id
    @GeneratedValue
    private Long id;
    private String Ville;
    @ManyToOne
    @JoinColumn(name="departement_id")
    private Departement departement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String ville) {
        Ville = ville;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }
}
