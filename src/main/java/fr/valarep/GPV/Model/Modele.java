package fr.valarep.GPV.Model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Modele {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nom;
    @OneToMany(mappedBy = "modele", fetch = FetchType.EAGER)
    private List<Voiture> voitures;
    @ManyToOne
    @JoinColumn(name="marque_id")
    private Marque marque;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }
}
