package fr.valarep.GPV.Controller;

import fr.valarep.GPV.Model.Reservation;
import fr.valarep.GPV.Repository.ReservationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
@CrossOrigin
@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private ReservationRepository reservationRepository;

    public ReservationController(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @PostMapping
    public void saveReservation(@RequestBody Reservation reservation){
        reservationRepository.save(reservation);
    }
    @GetMapping
    public Collection<Reservation> all() {

        return reservationRepository.findAll();
    }
    @GetMapping("/user/{user}")
    public Collection<Reservation> allByUser(@PathVariable  Long user) {
        return reservationRepository.getAllByUser_Id(user);
    }
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        reservationRepository.deleteById(id);
    }

    @GetMapping("/essai")
    public Collection<Reservation> getVoituresAProposer() {
        return reservationRepository.getAllByDateReservationAfter(LocalDateTime.now());
    }
}
