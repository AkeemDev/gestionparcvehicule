package fr.valarep.GPV.Controller;

import fr.valarep.GPV.Model.Modele;
import fr.valarep.GPV.Model.Voiture;
import fr.valarep.GPV.Repository.VoitureRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.NO_CONTENT;
@CrossOrigin
@RestController
@RequestMapping("/voitures")
public class VoitureController {

    private final VoitureRepository voitureRepository;
    @Autowired
    public VoitureController(VoitureRepository voitureRepository) {
        this.voitureRepository = voitureRepository;
    }


    @GetMapping
    public Collection<Voiture> all() {

        System.out.println(voitureRepository.findAll());
        return voitureRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Voiture> getPerson(@PathVariable Long id) {
        Voiture voiture = voitureRepository.findPersonById(id);


        if (voiture == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(voiture);
    }

    @PostMapping
    public void addVoiture(@RequestBody Voiture voiture) {
        voitureRepository.save(voiture);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void removePerson(@PathVariable Long id) {
        voitureRepository.deleteById(id);
    }


}
