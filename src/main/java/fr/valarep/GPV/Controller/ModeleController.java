package fr.valarep.GPV.Controller;

import fr.valarep.GPV.Model.Modele;
import fr.valarep.GPV.Repository.ModeleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/modeles")
public class ModeleController {

    private final ModeleRepository modeleRepository;
    @Autowired
    public ModeleController(ModeleRepository modeleRepository) {
        this.modeleRepository = modeleRepository;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public void addVoiture(@RequestBody Modele modele) {
        modeleRepository.save(modele);
    }



}
