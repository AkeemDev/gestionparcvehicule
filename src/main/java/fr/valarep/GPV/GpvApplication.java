package fr.valarep.GPV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class GpvApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpvApplication.class, args);
	}

}
